package lab7.pwr.edu.pl;

import lab7.pwr.edu.pl.DbUtilities;
import lab7.pwr.edu.pl.model.ResultSetParser;
import lab7.pwr.edu.pl.model.impl.ClientImpl;

import java.sql.*;
import java.util.ArrayList;

public class ClientExplorer {
    private DbUtilities dbUtilities;
    private String CONNECTION_STRING;
    private Connection connection;
    private final static String selectAllClients_text = "SELECT * FROM CLIENTS";
    private PreparedStatement selectAllClients;

    public ClientExplorer() {
        this.dbUtilities = new DbUtilities();
        this.CONNECTION_STRING = dbUtilities.CONNECTION_STRING;
    }

    public ArrayList<ClientImpl> getAllClients() throws SQLException {
        open();
        ResultSetParser parser = new ResultSetParser();
        ResultSet rs = selectAllClients.executeQuery();
        var clients = parser.convertToClients(rs);
        close();
        return clients;
    }

    private void open() throws SQLException {
        this.connection = DriverManager.getConnection(CONNECTION_STRING);
        initializePreparedStatements();
    }

    private void close() {
        try {
            if (this.connection != null) {
                this.connection.close();
                closeStatements();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initializePreparedStatements() throws SQLException {
        selectAllClients = connection.prepareStatement(selectAllClients_text);
    }

    private void closeStatements() throws SQLException {
        selectAllClients.close();
    }
}
