package lab7.pwr.edu.pl;

import lab7.pwr.edu.pl.model.impl.BenefitImpl;
import lab7.pwr.edu.pl.model.impl.ClientImpl;
import lab7.pwr.edu.pl.model.impl.PaidImpl;

import java.time.LocalDate;
import java.util.Scanner;

public class Main {

    public static void addMissingShowBalance(LocalDate actualDate) throws Exception {
        ClientExplorer clientExplorer = new ClientExplorer();
        var clients = clientExplorer.getAllClients();
        for (ClientImpl client : clients) {
            System.out.println("\nClient Id\t" + client.getId() + ":");
            ChargeAdder chargeAdder = new ChargeAdder(actualDate);
            chargeAdder.addMissingCharges(client);
            var benefits = client.getBenefits();
            for (BenefitImpl benefit : benefits) {
                PaymentManager paymentManager = new PaymentManager();
                int balance = paymentManager.getBilans(benefit);
                if (balance > 0) {
                    System.out.println("Benefit Id:\t" + benefit.getId() + "\tClient have to pay:\t" + balance);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        LocalDate actualDate = LocalDate.now();
        addMissingShowBalance(actualDate);
        System.out.println("Insert how much many do you want transfer");
        Scanner scanner = new Scanner(System.in);
        int moneyToTransfer = scanner.nextInt();
        System.out.println("Select Benefit Id");
        int benefitIdToTransferMoney = scanner.nextInt();
        PaidImpl paid = new PaidImpl(0, benefitIdToTransferMoney,moneyToTransfer, actualDate.toString() ); //id = 0 bo i tak autoinkrementacja, troche brzydko.
        paid.save();
        addMissingShowBalance(actualDate);
    }
}
