package lab7.pwr.edu.pl;

import lab7.pwr.edu.pl.model.impl.BenefitImpl;
import lab7.pwr.edu.pl.model.impl.ChargeImpl;
import lab7.pwr.edu.pl.model.impl.ClientImpl;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class ChargeAdder {
    private final LocalDate actualDate;

    public ChargeAdder(LocalDate actualDate) {
        this.actualDate = actualDate;
    }

    public class BenefitWithItsCharges {
        public BenefitImpl benefit;
        public List<ChargeImpl> charges;
        public long missingChargesCount;

        public BenefitWithItsCharges(BenefitImpl benefit, ArrayList<ChargeImpl> charges, long missingChargesCount) {
            this.benefit = benefit;
            this.charges = charges;
            this.missingChargesCount = missingChargesCount;
        }
    }

    private static long getMountsBetweenNow(String date, LocalDate actualDate) {
        return ChronoUnit.MONTHS.between(
                YearMonth.from(LocalDate.parse(date)),
                YearMonth.from(actualDate));
    }

    public void addMissingChargesForBenefit(BenefitWithItsCharges benefitChargesToModify) {
        BenefitImpl benefit = benefitChargesToModify.benefit;
        long missingChargesCount = benefitChargesToModify.missingChargesCount;
        ArrayList<ChargeImpl> charges = (ArrayList<ChargeImpl>) benefitChargesToModify.charges;
        LocalDate startDate = LocalDate.parse(benefitChargesToModify.benefit.getStartDate());
        LocalDate tempDate = startDate;
        while (missingChargesCount != 0) {
            try {
                if (! isChargeWithThisDate(tempDate, charges)) {
                    ChargeImpl chargeToBeSaved = new ChargeImpl(0,benefit.getId(), tempDate.toString());
                    chargeToBeSaved.save();
                    System.out.println(tempDate.toString());    //@ToDo Pisanie do pliku
                    missingChargesCount--;
                    tempDate = tempDate.plusMonths(1);
                }else {
                    tempDate = tempDate.plusMonths(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addMissingCharges(ClientImpl client) throws Exception {
        ArrayList<BenefitImpl> benefits = client.getBenefits();
        for (BenefitImpl benefit : benefits) {
            long monthsBetween = getMountsBetweenNow(benefit.getStartDate(), actualDate);
            List<ChargeImpl> charges = benefit.getCharges();
            if (monthsBetween != charges.size()) {
                if (monthsBetween - charges.size() < 0) {
                    throw new Exception("Some charges were sent to fast to Clients!!!");
                }
                System.out.println("!!! There should be " + monthsBetween + " Charges, but there are only " + charges.size());
                BenefitWithItsCharges benefitChargesToModify = new BenefitWithItsCharges(
                        benefit,
                        (ArrayList<ChargeImpl>) charges,
                        monthsBetween - charges.size());
                addMissingChargesForBenefit(benefitChargesToModify);
            }
        }
    }

    public boolean isTheSameMonth(LocalDate m1, LocalDate m2) {
        if (m1.getMonth().equals(m2.getMonth())) {
            return m1.getYear() == m2.getYear();
        }
        return false;
    }

    public boolean isChargeWithThisDate(LocalDate date, List<ChargeImpl> charges) throws Exception {
        ArrayList<ChargeImpl> foundCharges = new ArrayList<>(1);
        for (ChargeImpl charge : charges) {
            if (isTheSameMonth(date, LocalDate.parse(charge.getDeadline()))) {
                foundCharges.add(charge);
            }
        }
        if (foundCharges.size() == 0) {
            return false;
        }
        if (foundCharges.size() > 1) {
            throw new Exception("Finding Charge by Date found MORE than 1 result!");
        }
        return true;
    }
}
