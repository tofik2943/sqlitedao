package lab7.pwr.edu.pl.model.impl;

import lab7.pwr.edu.pl.DbUtilities;
import lab7.pwr.edu.pl.model.ResultSetParser;
import lab7.pwr.edu.pl.model.dao.ClientDAO;
import lab7.pwr.edu.pl.model.entities.ClientEntity;

import java.sql.*;
import java.util.ArrayList;

public class ClientImpl extends ClientEntity implements ClientDAO {
    private DbUtilities dbUtilities;
    private String CONNECTION_STRING;
    private Connection connection;

    private static final String selectClientBenefits_Text = "SELECT * FROM Benefit WHERE clientId = ?";
    private PreparedStatement selectClientBenefits;

    public ClientImpl(Integer id, String name, String surname) {
        super(id, name, surname);
        this.dbUtilities = new DbUtilities();
        this.CONNECTION_STRING = dbUtilities.CONNECTION_STRING;
    }


    @Override
    public ArrayList<BenefitImpl> getBenefits() throws SQLException {
        open();
        ResultSet rs = this.selectClientBenefits.executeQuery();
        ResultSetParser parser = new ResultSetParser();
        ArrayList<BenefitImpl> benefits = parser.convertToBenefits(rs);
        close();
        return benefits;
    }

    private void open() throws SQLException {
        this.connection = DriverManager.getConnection(CONNECTION_STRING);
        initializePreparedStatements();
    }

    private void close() {
        try {
            if (this.connection != null) {
                this.connection.close();
                closeStatements();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initializePreparedStatements() throws SQLException {
        selectClientBenefits = connection.prepareStatement(selectClientBenefits_Text);
        selectClientBenefits.setInt(1, this.getId());


    }

    private void closeStatements() throws SQLException {
        selectClientBenefits.close();
    }
}
