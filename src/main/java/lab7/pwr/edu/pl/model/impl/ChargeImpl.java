package lab7.pwr.edu.pl.model.impl;

import lab7.pwr.edu.pl.DbUtilities;
import lab7.pwr.edu.pl.model.dao.ChargeDAO;
import lab7.pwr.edu.pl.model.entities.ChargeEntity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ChargeImpl extends ChargeEntity implements ChargeDAO {
    private DbUtilities dbUtilities;
    private String CONNECTION_STRING;
    private Connection connection;
    private final static String insertCharge_text = "INSERT INTO Charge (benefitId, deadline)" +
            "VALUES(?, ?)";

    private PreparedStatement insertCharge;

    public ChargeImpl(Integer id, Integer benefitId, String deadline) {
        super(id, benefitId, deadline);
        this.dbUtilities = new DbUtilities();
        this.CONNECTION_STRING = dbUtilities.CONNECTION_STRING;
    }


    @Override
    public void save() throws SQLException {
        open();
        int modifiedRecordsCount = insertCharge.executeUpdate();
        close();
    }

    private void open() throws SQLException {
        this.connection = DriverManager.getConnection(CONNECTION_STRING);
        initializePreparedStatements();
    }

    private void close() {
        try {
            if (this.connection != null) {
                this.connection.close();
                closeStatements();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initializePreparedStatements() throws SQLException {
        insertCharge = connection.prepareStatement(insertCharge_text);
        insertCharge.setInt(1, this.getBenefitId());
        insertCharge.setString(2, this.getDeadline());

    }

    private void closeStatements() throws SQLException {
        insertCharge.close();
    }
}
