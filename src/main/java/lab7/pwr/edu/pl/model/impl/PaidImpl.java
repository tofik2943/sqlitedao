package lab7.pwr.edu.pl.model.impl;

import lab7.pwr.edu.pl.DbUtilities;
import lab7.pwr.edu.pl.model.dao.PaidDAO;
import lab7.pwr.edu.pl.model.entities.PaidEntity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PaidImpl extends PaidEntity implements PaidDAO {
    private DbUtilities dbUtilities;
    private String CONNECTION_STRING;
    private Connection connection;
    private final static String insertPaid_text = "INSERT INTO Paid (benefitId, amount, date)" +
            "VALUES(?, ?, ?)";
    private PreparedStatement insertPaid;

    public PaidImpl(Integer id, Integer benefitId, Integer amount, String date) {
        super(id, benefitId, amount, date);
        this.dbUtilities = new DbUtilities();
        this.CONNECTION_STRING = dbUtilities.CONNECTION_STRING;
    }


    @Override
    public void save() throws SQLException {
        open();
        int modifiedRecordsCount = insertPaid.executeUpdate();
        close();
    }

    private void open() throws SQLException {
        this.connection = DriverManager.getConnection(CONNECTION_STRING);
        initializePreparedStatements();
    }

    private void close() {
        try {
            if (this.connection != null) {
                this.connection.close();
                closeStatements();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initializePreparedStatements() throws SQLException {
        insertPaid = connection.prepareStatement(insertPaid_text);
        insertPaid.setInt(1, this.getBenefitId());
        insertPaid.setInt(2, this.getAmount());
        insertPaid.setString(3, this.getDate());
    }

    private void closeStatements() throws SQLException {
        insertPaid.close();
    }
}
