package lab7.pwr.edu.pl.model.entities;

public class ClientEntity {
    private Integer id;
    private String name;
    private String surname;

    public ClientEntity(Integer id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public Integer getId() {
        return id;
    }
}
