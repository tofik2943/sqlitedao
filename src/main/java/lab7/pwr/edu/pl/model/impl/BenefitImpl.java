package lab7.pwr.edu.pl.model.impl;

import lab7.pwr.edu.pl.DbUtilities;
import lab7.pwr.edu.pl.model.ResultSetParser;
import lab7.pwr.edu.pl.model.dao.BenefitDAO;
import lab7.pwr.edu.pl.model.entities.BenefitEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BenefitImpl extends BenefitEntity implements BenefitDAO {
    private DbUtilities dbUtilities;
    private String CONNECTION_STRING;
    private Connection connection;

    private static final String selectTariffPrice_Text =
            "SELECT distinct price from Tariff" +
                    " inner join Benefit B" +
                    " on Tariff.Id = B.tariffId" +
                    " where B.tariffId = ?";
    private static final String selectCharges_Text =
            "SELECT * FROM Charge WHERE Charge.benefitId = ?";
    private static final String selectPaids_Text = "SELECT * FROM Paid WHERE Paid.benefitId = ?";
    private PreparedStatement selectTariffPrice;
    private PreparedStatement selectCharges;
    private PreparedStatement selectPaids;

    public BenefitImpl(Integer id, Integer clientId, Integer tariffId, String address, Integer routerId, String startDate) {
        super(id, clientId, tariffId, address, routerId, startDate);
        this.dbUtilities = new DbUtilities();
        this.CONNECTION_STRING = dbUtilities.CONNECTION_STRING;
    }

    @Override
    public Integer getTariffPrice() throws Exception {
        Integer price = null;
        try {
            open();
            ResultSet rs = selectTariffPrice.executeQuery();
            ResultSetParser parser = new ResultSetParser();
            price = parser.convertToPrice(rs);
            close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        if (price == null) {
            throw new Exception("price is null");
        }
        return price;
    }

    @Override
    public List<ChargeImpl> getCharges() throws Exception {
        open();
        ResultSet rs = selectCharges.executeQuery();
        ResultSetParser parser = new ResultSetParser();
        ArrayList<ChargeImpl> charges = parser.convertToCharges(rs);
        close();
        return charges;
    }

    @Override
    public List<PaidImpl> getPaids() throws SQLException {
        open();
        ResultSet rs = selectPaids.executeQuery();
        ResultSetParser parser = new ResultSetParser();
        var paids = parser.convertToPaids(rs);
        close();
        return paids;
    }

    private void open() throws SQLException {
        this.connection = DriverManager.getConnection(CONNECTION_STRING);
        initializePreparedStatements();
    }

    private void close() {
        try {
            if (this.connection != null) {
                this.connection.close();
                closeStatements();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initializePreparedStatements() throws SQLException {
        selectTariffPrice = connection.prepareStatement(selectTariffPrice_Text);
        selectTariffPrice.setInt(1, this.getTariffId());
        selectCharges = connection.prepareStatement(selectCharges_Text);
        selectCharges.setInt(1, this.getId());
        selectPaids = connection.prepareStatement(selectPaids_Text);
        selectPaids.setInt(1, this.getId());
    }

    private void closeStatements() throws SQLException {
        selectTariffPrice.close();
        selectCharges.close();
        selectPaids.close();
    }
}

