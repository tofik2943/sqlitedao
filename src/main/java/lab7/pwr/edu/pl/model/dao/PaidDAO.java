package lab7.pwr.edu.pl.model.dao;

import java.sql.SQLException;

public interface PaidDAO {
    public void save() throws SQLException;
}
