package lab7.pwr.edu.pl.model.entities;


public class ChargeEntity {
    private Integer Id;
    private Integer benefitId;
    private String deadline;

    public ChargeEntity(Integer id, Integer benefitId, String deadline) {
        Id = id;
        this.benefitId = benefitId;
        this.deadline = deadline;
    }

    public Integer getId() {
        return Id;
    }

    public Integer getBenefitId() {
        return benefitId;
    }

    public String getDeadline() {
        return deadline;
    }
}
