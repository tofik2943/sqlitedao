package lab7.pwr.edu.pl.model.dao;

import lab7.pwr.edu.pl.model.impl.ChargeImpl;
import lab7.pwr.edu.pl.model.impl.PaidImpl;

import java.sql.SQLException;
import java.util.List;

public interface BenefitDAO {
    public Integer getTariffPrice() throws Exception;

    public List<ChargeImpl> getCharges() throws Exception;
    public List<PaidImpl> getPaids() throws SQLException;
}
