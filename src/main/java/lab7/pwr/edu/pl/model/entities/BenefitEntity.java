package lab7.pwr.edu.pl.model.entities;

public class BenefitEntity {
    private Integer Id;
    private Integer clientId;
    private Integer tariffId;
    private String address;
    private Integer routerId;
    private String startDate;

    public BenefitEntity(Integer id, Integer clientId, Integer tariffId, String address, Integer routerId, String startDate) {
        this.Id = id;
        this.clientId = clientId;
        this.tariffId = tariffId;
        this.address = address;
        this.routerId = routerId;
        this.startDate = startDate;
    }

    public Integer getId() {
        return Id;
    }


    public Integer getTariffId() {
        return tariffId;
    }

    public String getStartDate() {
        return startDate;
    }
}
