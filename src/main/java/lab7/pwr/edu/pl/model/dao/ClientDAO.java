package lab7.pwr.edu.pl.model.dao;

import lab7.pwr.edu.pl.model.impl.BenefitImpl;

import java.sql.SQLException;
import java.util.List;

public interface ClientDAO {

    public List<BenefitImpl> getBenefits() throws SQLException;

}
