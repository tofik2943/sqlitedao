package lab7.pwr.edu.pl.model;

import lab7.pwr.edu.pl.model.entities.ClientEntity;
import lab7.pwr.edu.pl.model.impl.BenefitImpl;
import lab7.pwr.edu.pl.model.impl.ChargeImpl;
import lab7.pwr.edu.pl.model.impl.ClientImpl;
import lab7.pwr.edu.pl.model.impl.PaidImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ResultSetParser {

    public ArrayList<ClientImpl> convertToClients(ResultSet rs) throws SQLException {
        ArrayList<ClientImpl> clients = new ArrayList<>();
        while (rs.next()) {
            int id = rs.getInt("Id");
            String name = rs.getString("name");
            String surname = rs.getString("surname");
            clients.add(new ClientImpl(id, name, surname));
        }
        return clients;
    }

    public ArrayList<BenefitImpl> convertToBenefits(ResultSet rs) throws SQLException {
        ArrayList<BenefitImpl> benefits = new ArrayList<>();
        while (rs.next()) {
            Integer id = rs.getInt("Id");
            Integer clientId = rs.getInt("clientId");
            Integer tariffId = rs.getInt("tariffId");
            String address = rs.getString("address");
            Integer routerId = rs.getInt("routerId");
            String startDate = rs.getString("startDate");
            benefits.add(new BenefitImpl(id, clientId, tariffId, address, routerId, startDate));
        }
        return benefits;
    }

    public Integer convertToPrice(ResultSet rs) throws Exception {
        var prices = new ArrayList<Integer>();
        while (rs.next()) {
            var price = rs.getInt("price");
            prices.add(price);
        }
        if (prices.size() > 1) {
            System.out.println("price list shouldn be size larger than 1");
            throw new Exception("prices contains more than one price");
        }
        return prices.get(0);
    }

    public ArrayList<ChargeImpl> convertToCharges(ResultSet rs) throws SQLException {
        var charges = new ArrayList<ChargeImpl>();
        while (rs.next()) {
            Integer Id = rs.getInt("Id");
            Integer benefitId = rs.getInt("benefitId");
            String deadline = rs.getString("deadline");
            charges.add(new ChargeImpl(Id, benefitId, deadline));
        }
        return charges;
    }

    public ArrayList<PaidImpl> convertToPaids(ResultSet rs) throws SQLException{
        var paids = new ArrayList<PaidImpl>();
        while (rs.next()){
            Integer Id = rs.getInt("Id");
            Integer benefitId = rs.getInt("benefitId");
            Integer amount = rs.getInt("amount");
            String date = rs.getString("date");
            paids.add(new PaidImpl(Id, benefitId, amount, date));
        }
        return paids;
    }
}
