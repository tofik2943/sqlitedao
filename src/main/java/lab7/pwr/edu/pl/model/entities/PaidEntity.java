package lab7.pwr.edu.pl.model.entities;

import java.time.LocalDate;

public class PaidEntity {
    private Integer Id;
    private Integer benefitId;
    private Integer amount;
    private String date;

    public PaidEntity(Integer id, Integer benefitId, Integer amount, String date) {
        this.Id = id;
        this.benefitId = benefitId;
        this.amount = amount;
        this.date = date;
    }

    public Integer getBenefitId() {
        return benefitId;
    }

    public Integer getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }
}
