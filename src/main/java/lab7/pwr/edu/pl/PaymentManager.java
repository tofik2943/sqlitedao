package lab7.pwr.edu.pl;

import lab7.pwr.edu.pl.model.impl.BenefitImpl;
import lab7.pwr.edu.pl.model.impl.PaidImpl;

import java.sql.SQLException;

public class PaymentManager {

    private int countAmountThatShouldBePaid(BenefitImpl benefit) throws Exception {
        int chargesAmount = benefit.getCharges().size();
        int pricePerCharge = benefit.getTariffPrice();
        return chargesAmount * pricePerCharge;
    }

    private int countAmountThatWasPaid(BenefitImpl benefit) throws SQLException {
        var paids = benefit.getPaids();
        int moneyAlreadyPaid = 0;
        for (PaidImpl paid : paids) {
            moneyAlreadyPaid += paid.getAmount();
        }
        return moneyAlreadyPaid;
    }

    public int getBilans(BenefitImpl benefit) throws Exception {
        return countAmountThatShouldBePaid(benefit) - countAmountThatWasPaid(benefit);
    }
}
